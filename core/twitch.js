module.exports = class Twitch {

    constructor(client_id) {
        this.axios = require('axios');
        this.client_id = client_id;
    }

    getUserId(username) {
        return new Promise(resolve => {
            let inst = this.axios.create();
            inst.defaults.headers.common['Client-ID'] = this.client_id;

            let channel = 'channel=';

            inst.get('https://api.twitch.tv/kraken/users/' + username)
                .then(res => {
                    resolve(res.data);
                })
                .catch(err => {
                    console.log(err);
                    if (err) throw err;
                });
        });
    }

    getStatusOfStreams(streams_id) {
        let inst = this.axios.create();
        inst.defaults.headers.common['Client-ID'] = this.client_id;

        let channel = 'channel=' + id;
        inst.get('https://api.twitch.tv/kraken/streams/?' + channel)
            .then(res => {
                console.log(res.data);
            })
            .catch(err => {
                console.log(err);
                if (err) throw err;
            })
    }

    getUserFollows(login) {
        return new Promise((resolve, reject) => {
            let inst = this.axios.create();
            inst.defaults.headers.common['Client-ID'] = this.client_id;
            let promises = [];


            let count = 100;
            let total = 101;
            let step = 0;
            let offset = count * step;

            do {
                promises.push(inst.get('https://api.twitch.tv/kraken/users/' + login + '/follows/channels?limit=100&offset=' + offset));
            } while (total > offset);

            Promise.all(promises).then(res => {
                resolve(res);
            }).catch(err => {
                reject(err);
            });
        });

    }
};
