module.exports =  class Bot {

    constructor(token, updates, twitch) {
        this.telegram = require('telegram-bot-api');
        this.token = token;
        this.updates = updates;
        this.twitch = twitch;
        this.listUsers = [];
        this.lastStreams= [];
        this.onlineStreams= [];
        this.allStreamsList = [];
    }

    init() {
        this.api = new this.telegram({
            token: this.token,
            updates: {
                enabled: this.updates
            }
        });
    }

    repeatMessages() {
        this.api.on('message', mess => {
            if (mess.text === '/live') return;
            
            this.api.sendMessage({
                chat_id: mess.chat.id,
                text: mess.text
            });
        });
    }

    reverseMess() {
        this.api.on('message', mess => {
            this.api.sendMessage({
                chat_id: mess.chat.id,
                text: mess.text.split("").reverse().join("")
            })
            .catch(err => {
                console.log(err);
            });
        });
    }

    twitchOnlineStreams() {
        this.api.on('message', mess => {
            if (mess.text === '/live') {
                let inst = this.axios.create();

                inst.defaults.headers.common['Authorization'] = 'OAuth ' + this.twitchToken;
                inst.defaults.headers.common['Client-ID'] = this.tw_client_id;

                inst.get('https://api.twitch.tv/kraken/streams/followed')
                .then(res => {
                    this.api.sendMessage({
                        chat_id: mess.chat.id,
                        text: "Всего онлайн: " + res.data._total + '\n\r'
                    })
                    .then( () => {
                        for (let item of res.data.streams) {
                            let text = '\n> ' + item.channel.display_name;
                            text += '\nИгра: ' + item.channel.game;
                            text += '\nКанал: ' + item.channel.url;
                            text += '\nЗрители: ' + item.viewers + '\n\r';

                            this.api.sendPhoto({
                                chat_id: mess.chat.id,
                                caption: text,
                                photo: item.channel.logo
                            });
                        }

                    });


                })
                .catch(err => {
                    console.log(err);
                });

            }
        });
    }

    getLiveStreams() {
        return new Promise((resolve, reject) => {
            let inst = this.axios.create();
            this.currentList = [];

            inst.defaults.headers.common['Authorization'] = 'OAuth ' + this.twitchToken;
            inst.defaults.headers.common['Client-ID'] = this.tw_client_id;

            inst.get('https://api.twitch.tv/kraken/streams/followed')
                .then(res => {
                    for (let item of res.data.streams) {
                        this.currentList.push(item._id);
                    }
                    resolve(res.data);

                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                });
        });
    }

    alertAboutNewStream(minutes = 1) {
        setInterval(() => {
            this.getLiveStreams()
            .then(res => {
                for (let item of res.streams) {
                    if (this.streamList.indexOf(item._id) !== -1) continue;

                    let text = '\n> ' + item.channel.display_name + ' начал стрим.';
                    text += '\nИгра: ' + item.channel.game;
                    text += '\nКанал: ' + item.channel.url;
                    text += '\nЗрители: ' + item.viewers + '\n\r';

                    this.api.sendPhoto({
                        chat_id: this.telegram_id,
                        caption: text,
                        photo: item.channel.logo
                    });
                }

                this.streamList = this.currentList;
            })
            .catch(err => {
                console.log(err);
            })
        }, minutes * 60000)
    }

    activateUser() {
        this.api.on('message', mess => {
            let login = false;
            let temp = mess.text.split(" ");

            let command = temp[0];
            if (command === '/user')
                login = temp[1];

            let temp_arr  = [];
            for (let item of this.listUsers) {
                temp_arr.push(item.login);
            }
            if (temp_arr.indexOf(login) === -1 && login) {
                let obj = {
                    login,
                    counts: 1,
                    subs: [mess.chat.id],
                    streamsList: []
                };
                this.listUsers.push(obj);
                this.api.sendMessage({
                    chat_id: mess.chat.id,
                    text: 'Вы будете получать уведомления о стримах, на которые подписан пользователя: ' + login
                });
            } else {
                for (let index in this.listUsers) {
                    if (this.listUsers[index].login === login) {
                        if (this.listUsers[index].subs.indexOf(mess.chat.id) === -1) {
                            this.listUsers[index].counts++;
                            this.listUsers[index].subs.push(mess.chat.id);
                            this.api.sendMessage({
                                chat_id: mess.chat.id,
                                text: 'Вы будете получать уведомления о стримах, на которые подписан пользователя: ' + login
                            });
                        } else {
                            this.api.sendMessage({
                                chat_id: mess.chat.id,
                                text: 'Вы уже подписаны на этого пользователя'
                            });
                        }
                    }
                }
            }

            console.log(this.listUsers);
        });
    }

    deactivateUser() {
        this.api.on('message', mess => {
            let login = false;
            let temp = mess.text.split(" ");

            let command = temp[0];
            if (command === '/unsub')
                login = temp[1];

            let temp_arr  = [];
            for (let item of this.listUsers) {
                temp_arr.push(item.login);
            }

            if (temp_arr.indexOf(login) === -1 && login) {
                this.api.sendMessage({
                    chat_id: mess.chat.id,
                    text: 'вы не подписаны на данного пользователя'
                });
            } else {
                for (let index in this.listUsers) {
                    if (this.listUsers[index].login === login) {
                        if (this.listUsers[index].subs.indexOf(mess.chat.id) === -1) {
                            this.api.sendMessage({
                                chat_id: mess.chat.id,
                                text: "вы не подписаны на данного пользователя"
                            });
                        } else {
                            for (let sub in this.listUsers[index].subs) {
                                if (this.listUsers[index].subs[sub] === mess.chat.id) {
                                    this.listUsers[index].subs.splice(sub, 1);
                                    this.listUsers[index].counts--;
                                    this.api.sendMessage({
                                        chat_id: mess.chat.id,
                                        text: "Вы отписались от пользователя: " + login
                                    });
                                    if (this.listUsers[index].counts === 0)
                                        this.listUsers.splice(index, 1);
                                }

                            }
                        }

                    }
                }
            }

            console.log(this.listUsers);
        });
    }
};