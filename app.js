let Bot = require('./core/bot');
let conf = require('./core/config');
let DB = require('./core/db');
let Twitch = require('./core/twitch');

let mysql_conf = {
    host: conf.db_host,
    user: conf.db_user,
    password: conf.db_pass,
    database: conf.db_name
};

let twitch = new Twitch(conf.twitch_client_id);

let app = new Bot(conf.token, true, twitch);

app.init();
app.activateUser();
app.deactivateUser();

twitch.getUserFollows('exiragor').then(res => {
    console.log(res);
}).catch(err => {
    console.log(err);
    throw err;
});




// app.init();
// app.repeatMessages();
// app.reverseMess();
// app.twitchOnlineStreams();
// app.alertAboutNewStream(10);